package coolc.codegen;

import coolc.parser.*;
import coolc.infrastructure.*;
import coolc.ast.*;
import coolc.codegen.*;
import java.util.*;

public class CodeGenerator {
	private SymTable _symTable;
    private Program _root;
    private StringBuilder _declarations;
    private StringBuilder _body;
    private StringBuilder _init;
    private StringBuilder _strings;
    private StringBuilder _definitions;
    private String _autoGen;
    private int _string_index;
    private int _variable_index;
    //<Id, Type>
    private HashMap<String, String> _locals;
    //<Id, Type>
    private HashMap<String, Integer> _str_lengths;
    private String _if_true_label;
    private String _if_false_label;
    private ArrayList<LlvmClass> _classes;
    private String _main_class;
    private String _main_type;

    public StringBuilder getDeclarations() {
        return _declarations;
    }

    public StringBuilder getBody() {
        return _body;
    }

    public StringBuilder getInit() {
        return _init;
    }

    public StringBuilder getStrings() {
        return _strings;
    }

    public StringBuilder getDefinitions() {
        return _definitions;
    }

    public String getAutoGen() {
        return _autoGen;
    }

    public CodeGenerator(Program root) {
        _root = root;
        _symTable = new SymTable(root);
        _declarations = new StringBuilder();
        _body = new StringBuilder();
        _init = new StringBuilder();
        _strings = new StringBuilder();
        _definitions = new StringBuilder();
        _string_index = 0;
        _variable_index = 0;
        _locals = new HashMap<String, String>();
        _str_lengths = new HashMap<String, Integer>();
        _if_true_label = "";
        _if_false_label = "";
        _classes = new ArrayList<LlvmClass>();
        _classes.add(new LlvmClass("IO"));
        _main_class = "";
        _main_type = "";
        readTree(_root);
        _autoGen = autogenerateCode();
    }

    private String autogenerateCode() {
        /* Código que aparece siempre */
        String aux = "";
        aux += "%Object = type { i8* }\n";
        aux += "%IO = type { i8* }\n";
        aux += 
            "define i32 @main() {\n" +
                //init.toString() + "\n" +
                "\t%obj = call %" + _main_class + "* @new" + _main_class + "()\n" +
                "\tcall %" + _main_type + "* @"+ _main_class + "_main(%" + _main_class + "* %obj)\n" +
                "\tret i32 0\n" +
            "}\n";
        aux += "declare %Object* @Object_abort(%Object*)\n";
        aux += "declare i8* @Object_type_name(%Object*)\n";
        aux += "declare %IO* @IO_out_string(%IO*, i8*)\n";
        aux += "declare %IO* @IO_out_int(%IO*, i32 )\n";
        aux += "declare i8* @IO_in_string(%IO*)\n";
        aux += "declare i32 @IO_in_int(%IO*)\n";
        aux += "declare i32 @String_length(i8*)\n";
        aux += "declare i8* @String_concat(i8*, i8*)\n";
        aux += "declare i8* @String_substr(i8*, i32, i32 )\n";
        aux += "declare i32 @strcmp(i8*, i8*)\n";
        aux += "declare i8* @malloc(i64)\n";

        /* IO */
        aux += "@.type.IO = private constant [3 x i8] c\"IO\\00\"\n";
        aux += 
            "define %IO* @newIO() {\n" +
                "\t%vptr = call i8* @malloc( i64 ptrtoint (%IO* getelementptr (%IO* null, i32 1) to i64) )\n" +
                "\t%ptr = bitcast i8* %vptr to %IO*\n" +
                "\t%typePtr = getelementptr %IO* %ptr, i32 0, i32 0\n" +
                "\tstore i8* bitcast( [3 x i8]* @.type.IO to i8*), i8** %typePtr\n" +
                "\tret %IO* %ptr\n" +
            "}\n";

        /* Object */
        aux += "@.type.Object = private constant [7 x i8] c\"Object\\00\"\n";
        aux += 
            "define %Object* @newObject() {\n" +
                "\t%vptr = call i8* @malloc( i64 ptrtoint (%Object* getelementptr (%Object* null, i32 1) to i64) )\n" +
                "\t%ptr = bitcast i8* %vptr to %Object*\n" +
                "\t%typePtr = getelementptr %Object* %ptr, i32 0, i32 0\n" +
                "\tstore i8* bitcast( [7 x i8]* @.type.Object to i8*), i8** %typePtr\n" +
                "\tret %Object* %ptr\n" +
            "}\n";

        /* Operaciones binarias */
        aux += 
            "define i32 @_add(i32 %a, i32 %b) {\n" +
                "\t%_tmp_1 = add i32 %a, %b\n" +
                "\tret i32 %_tmp_1\n" +
            "}\n";

        aux += 
            "define i32 @_substract(i32 %a, i32 %b) {\n" +
                "\t%_tmp_1 = sub i32 %a, %b\n" +
                "\tret i32 %_tmp_1\n" +
            "}\n";

        aux += 
            "define i32 @_multiply(i32 %a, i32 %b) {\n" +
                "\t%_tmp_1 = mul i32 %a, %b\n" +
                "\tret i32 %_tmp_1\n" +
            "}\n";

        aux += 
            "define i32 @_divide(i32 %a, i32 %b) {\n" +
                "\t%_tmp_1 = sdiv i32 %a, %b\n" +
                "\tret i32 %_tmp_1\n" +
            "}\n";

        aux += 
            "define i1 @_less_than(i32 %a, i32 %b) {\n" +
                "\t%_tmp_1 = icmp slt i32 %a, %b\n" +
                "\tret i1 %_tmp_1\n" +
            "}\n";

        aux += 
            "define i1 @_less_or_equals(i32 %a, i32 %b) {\n" +
                "\t%_tmp_1 = icmp sle i32 %a, %b\n" +
                "\tret i1 %_tmp_1\n" +
            "}\n";

        aux += 
            "define i1 @_equals(i32 %a, i32 %b) {\n" +
                "\t%_tmp_1 = icmp eq i32 %a, %b\n" +
                "\tret i1 %_tmp_1\n" +
            "}\n";

        /* Operaciones unarias */
        aux +=
            "define i32 @_negate(i32 %a) {\n" +
                "\t%_neg_a = xor i32 %a, -1\n" +
                "\tret i32 %_neg_a\n" +
            "}\n";

        aux += 
            "define i1 @_not(i1 %a) {\n" +
                "\t%_tmp_1 = xor i1 %a, 1\n" +
                "\tret i1 %_tmp_1\n" +
            "}\n";

        aux += "@.emptystr = private unnamed_addr constant [1 x i8] c\"\\00\"\n";

        return aux;
    }

    private boolean checkTypes(String valueType, String slotType, Node node) {
        if( valueType != null && !_symTable.instanceOf(valueType, slotType)) {
            if(node != null) {
                _symTable.getErrors().add(new SemanticError(String.format("Expected %s, received %s", slotType, valueType), node.getPos()));

            }

            return false;
        }
        return true;
    }

    private ClassScope findClass(String name) {
        ClassScope scope = _symTable.findClass(name);


        return scope;
    }

    private String classIfExists(String name, Scope scope) {

        return name.equals("SELF_TYPE") ? scope.getClassScope().getClassType() : (findClass(name) != null ? name : null);
    }

    private void readTree(Program root)  {
        for(ClassDef c: root) {
            ClassScope scope = _symTable.findClass(c.getType());
            assert scope != null : "Fatal error, this is impossible!";
            LlvmClass llvm_class = new LlvmClass(c.getType());
            _classes.add(llvm_class);
            /* Imprimir código de definición de tipo. */
            //Ojo con herencia, falta agregarlo!
            _declarations.append("%").append(c.getType()).append(" = type {\n");
            _declarations.append("i8*,\n");
            /* Imprimir código de definición de clase. */
            _definitions.append("define ").append(coolToLlvmType(c.getType())).append(" @new");
            _definitions.append(c.getType()).append("() {\n");
            //; MiClase* ptr = (MiClase*)malloc(sizeof(MiClase))
            _definitions.append("%vptr = call i8* @malloc( i64 ptrtoint (").append(coolToLlvmType(c.getType()));
            _definitions.append(" getelementptr (").append(coolToLlvmType(c.getType()));
            _definitions.append(" null, i32 1) to i64) )\n");
            _definitions.append("%ptr = bitcast i8* %vptr to ").append(coolToLlvmType(c.getType())).append("\n");
            //; ptr->type = @.type.MiClase
            _definitions.append("%typePtr = getelementptr ").append(coolToLlvmType(c.getType()));
            _definitions.append(" %ptr, i32 0, i32 0\n");
            _definitions.append("store i8* bitcast( [8 x i8]* @.type.").append(c.getType());
            _definitions.append(" to i8*), i8** %typePtr\n");
            for(Feature feature: c.getBody()) {
                if(feature instanceof Method) {
                    Method method = (Method)feature;
                    String returnType = method.getType();

                    if( "SELF_TYPE".equals(returnType) ) {
                        returnType = scope.getClassType();
                    }
                    llvm_class.addMethod(method.getName());
                    if(method.getName().equals("main")) {
                        _main_class = scope.getClassType();
                        _main_type = returnType;
                    }

                    _body.append("define ").append(coolToLlvmType(returnType)).append(" @");
                    _body.append(scope.getClassType()).append("_").append(method.getName());
                    _body.append("(").append(coolToLlvmType(scope.getClassType())).append(" %self");
                    for(Variable var : method.getParams()) {
                        _body.append(", ").append(coolToLlvmType(var.getType()));
                        _body.append(" %").append(var.getId());
                    }
                    _body.append("){\n");
                    String node = readNode(method.getBody(), method.getScope(), _body);
                    String var_name = generateVariableId();
                    _body.append("%").append(var_name).append(" = bitcast ").append(coolToLlvmType(method.getBody().getExprType()));
                    _body.append(" %").append(node).append(" to ").append(coolToLlvmType(returnType)).append("\n");
                    _locals.put(var_name, returnType);
                    _body.append("ret ").append(coolToLlvmType(returnType)).append(" %").append(var_name).append("\n");
                    _body.append("}\n");

                } 
                else if(feature instanceof Variable) {
                    Variable var =  (Variable)feature;
                    String returnId = var.getType();

                    llvm_class.addAttribute(var.getId());
                    int attr_index = llvm_class.getAttributeIndex(var.getId());
                    if(returnId.equals("Int")) {
                        _declarations.append("i32,\n");
                        _definitions.append("%").append(var.getId()).append(" = getelementptr ");
                        _definitions.append(coolToLlvmType(scope.getClassType())).append(" %ptr, i32 0, i32 ");
                        _definitions.append(attr_index).append("\n");
                        _definitions.append("store i32 0, i32* %").append(var.getId()).append("\n");
                    }
                    else if(returnId.equals("Bool")) {
                        _declarations.append("i1,\n");
                        _definitions.append("%").append(var.getId()).append(" = getelementptr ");
                        _definitions.append(coolToLlvmType(scope.getClassType())).append(" %ptr, i32 0, i32 ");
                        _definitions.append(attr_index).append("\n");
                        _definitions.append("store i1 false, i1* %").append(var.getId()).append("\n");
                    }
                    else if(returnId.equals("String")) {
                        _declarations.append("i8*,\n");
                        _definitions.append("%").append(var.getId()).append(" = getelementptr ");
                        _definitions.append(coolToLlvmType(scope.getClassType())).append(" %ptr, i32 0, i32 ");
                        _definitions.append(attr_index).append("\n");
                        _definitions.append("store i8* @.emptystr, i8** %").append(var.getId()).append("\n");
                        _str_lengths.put(var.getId(), 1);
                    }
                    else {
                        _declarations.append(coolToLlvmType(var.getType())).append(",\n");
                    }
                    if(var.getValue() != null) {
                        String node = readNode(var.getValue(), scope, _definitions);
                        if(returnId.equals("Int")) {
                            _definitions.append("%").append(var.getId()).append(" = getelementptr ");
                            _definitions.append(coolToLlvmType(scope.getClassType())).append(" %ptr, i32 0, i32 ");
                            _definitions.append(attr_index).append("\n");
                            _definitions.append("store i32 0, i32* %").append(node).append("\n");
                        }
                        else if(returnId.equals("Bool")) {
                            _definitions.append("%").append(var.getId()).append(" = getelementptr ");
                            _definitions.append(coolToLlvmType(scope.getClassType())).append(" %ptr, i32 0, i32 ");
                            _definitions.append(attr_index).append("\n");
                            _definitions.append("store i1 false, i1* %").append(node).append("\n");
                        }
                        else if(returnId.equals("String")) {
                            int l = _str_lengths.get(node);
                            _definitions.append("%").append(var.getId()).append(" = getelementptr ");
                            _definitions.append(coolToLlvmType(scope.getClassType())).append(" %ptr, i32 0, i32 ");
                            _definitions.append(attr_index).append("\n");
                            _definitions.append("store i8* %").append(node).append(", i8** %").append(var.getId()).append("\n");
                            _str_lengths.put(var.getId(), l);
                        }
                        else {
                            //Inicialización de objetos de un tipo no-básico.
                            _definitions.append("%").append(var.getId()).append(" = getelementptr ");
                            _definitions.append(coolToLlvmType(scope.getClassType())).append(" %ptr, i32 0, i32 ");
                            _definitions.append(attr_index).append("\n");
                            _definitions.append("store ").append(coolToLlvmType(returnId)).append(" %").append(node);
                            _definitions.append(", ").append(coolToLlvmType(returnId)).append("* %").append(var.getId()).append("\n");
                        }
                    }
                }
            }
            _declarations.deleteCharAt(_declarations.length() - 2);
            _declarations.append("}\n");

            _definitions.append("ret ").append(coolToLlvmType(c.getType())).append(" %ptr\n");
            _definitions.append("}\n");

            //Escribimos la variable con el tipo creado.
            int l = c.getType().length() + 1;
            _declarations.append("@.type.").append(c.getType()).append(" = private constant [");
            _declarations.append(l).append(" x i8] c\"").append(c.getType()).append("\\00\"\n");
        }
    }

    private String readNode(Expr e, Scope scope, StringBuilder buffer) {

        assert e != null : "node is null";

        String returnId = null;

        if(e instanceof Block) {

            for(Expr child: ((Block)e).getStatements()) {
                returnId = readNode(child, scope, buffer);
            }
        }
        else if(e instanceof WhileExpr) {
            // Nothing done here :(.
        }
        else if(e instanceof AssignExpr) {

            AssignExpr expr = (AssignExpr)e;

            Field field = scope.getField(expr.getId());

            String value_node = readNode(expr.getValue(), scope, buffer);

            if(field.getParent() instanceof ClassScope) {
                ClassScope c_scope = (ClassScope)field.getParent();

                for(int i = 0; i < _classes.size(); i++) {
                    if(_classes.get(i).getName().equals(c_scope.getClassType())) {
                        int attr_index = _classes.get(i).getAttributeIndex(expr.getId());
                        buffer.append("%").append(expr.getId()).append(" = getelementptr ");
                        buffer.append(coolToLlvmType(c_scope.getClassType())).append(" %self, i32 0, i32 ");
                        buffer.append(attr_index).append("\n");
                        buffer.append("store ").append(coolToLlvmType(expr.getExprType())).append(" %").append(value_node);
                        buffer.append(", ").append(coolToLlvmType(expr.getExprType())).append("* %").append(expr.getId()).append("\n");
                    }
                }
            }
            else if(field.getParent() instanceof MethodScope) {
                buffer.append("%").append(expr.getId()).append(" = ").append(coolToLlvmType(field.getType()));
                buffer.append(" %").append(value_node).append("\n");
            }
            else {
                //Let y case.
            }
            returnId = value_node;

        }        
        else if(e instanceof DispatchExpr) {
            DispatchExpr call = (DispatchExpr)e;

            String exprType;

            ClassScope calleeScope;


            String callee_node = "";
            if(call.getExpr() == null) {
                callee_node = "self";
                calleeScope = scope.getClassScope();
            }
            else {
                callee_node = readNode(call.getExpr(), scope, buffer);
                if(callee_node.equals("self")) {
                    calleeScope = scope.getClassScope();
                }
                else {
                    calleeScope = _symTable.findClass(_locals.get(callee_node));
                }
            }


            exprType = calleeScope != null ? calleeScope.getClassType() : null;

            if( calleeScope != null && call.getType() != null ) {

                ClassScope castScope = _symTable.findClass(call.getType());

                if(castScope == null) {
                    calleeScope = null;
                }
                else if (!_symTable.instanceOf(calleeScope.getClassType(), call.getType())) {
                    calleeScope = null;
                }
                else {
                    calleeScope = castScope;
                }

            }
            
            MethodScope method = calleeScope != null ? calleeScope.getMethod(call.getName()) : null ;

            if(method == null) {
            }
            else {

                int paramCount = method.getParams().size();
                int argCount = call.getArgs().size();
                StringBuilder callSignature = new StringBuilder();
                String call_var_name = generateVariableId();
                String signature_code;
                String callType = fixMainType(call.getExprType());
                signature_code = "call " + coolToLlvmType(callType)
                    + " @" + method.getClassScope().getClassType() + "_" + call.getName();
                callSignature.append(signature_code).append("(");
                String param_id = "";
                if(call.getExpr() == null || callee_node.equals("self")) {
                    if(!method.getClassScope().getClassType().equals(calleeScope.getClassType())) {
                        String aux = generateVariableId();
                        buffer.append("%").append(aux).append(" = bitcast ").append(coolToLlvmType(calleeScope.getClassType()));
                        buffer.append(" %self to ").append(coolToLlvmType(method.getClassScope().getClassType())).append("\n");
                        param_id = aux;
                    }
                    else {
                        param_id = "self";
                    }
                    callSignature.append(coolToLlvmType(method.getClassScope().getClassType())).append(" %").append(param_id);
                }
                else {
                    if(!method.getClassScope().getClassType().equals(_locals.get(callee_node))) {
                        String aux = generateVariableId();
                        buffer.append("%").append(aux).append(" = bitcast ").append(coolToLlvmType(_locals.get(callee_node)));
                        buffer.append(" %").append(callee_node).append(" to ").append(coolToLlvmType(method.getClassScope().getClassType())).append("\n");
                        param_id = aux;
                    }
                    else {
                        param_id = callee_node;
                    }
                    callSignature.append(coolToLlvmType(method.getClassScope().getClassType())).append(" %").append(param_id);
                }

                boolean hasErrors = false;

                if(paramCount != argCount) {
                    hasErrors = true;                    
                }
                for(int i = 0; i < argCount; i++) {

                    Expr arg = call.getArgs().get(i);
                    String argId = readNode(arg, scope, buffer);
                    String paramName = method.getParams().get(i);
                    String methArgType = method.getField(paramName).getType();
                    if(!methArgType.equals(arg.getExprType())) {
                        String aux = generateVariableId();
                        buffer.append("%").append(aux).append(" = bitcast ").append(coolToLlvmType(arg.getExprType()));
                        buffer.append(" %").append(argId).append(" to ").append(coolToLlvmType(methArgType)).append("\n");
                        argId = aux;
                    }
                    callSignature.append(", ").append(coolToLlvmType(methArgType)).append(" %").append(argId);

                    if(i < paramCount) {
                    }
                    
                }

                callSignature.append(")\n");
                buffer.append("%").append(call_var_name).append(" = ").append(callSignature.toString());
                _locals.put(call_var_name, callType);
                returnId = call_var_name;
            }
            

        }
        else if(e instanceof IfExpr) {

            IfExpr expr = (IfExpr)e;


            //Variables a usar.
            String ifTrue_name = generateVariableId();
            String ifFalse_name = generateVariableId();
            String ifEnd_name = generateVariableId();
            String ifResult_name = generateVariableId();
            //If_cond:
            String condNode = readNode(expr.getCond(), scope, buffer);
            buffer.append("br i1 %").append(condNode).append(", label %").append(ifTrue_name);
            buffer.append(", label %").append(ifFalse_name).append("\n");
            //If (true):
            buffer.append(ifTrue_name).append(":\n");
            _if_true_label = ifTrue_name;
            String trueNode = readNode(expr.getTrue(), scope, buffer);
            buffer.append("br label %").append(ifEnd_name).append("\n");
            //If (false):
            buffer.append(ifFalse_name).append(":\n");
            _if_false_label = ifFalse_name;
            String falseNode = readNode(expr.getFalse(), scope, buffer);
            buffer.append("br label %").append(ifEnd_name).append("\n");
            //If_end:
            buffer.append(ifEnd_name).append(":\n");
            String resultType = fixMainType(expr.getExprType());
            buffer.append("%").append(ifResult_name).append(" = phi ").append(coolToLlvmType(resultType));
            buffer.append(" [ %").append(trueNode);
            buffer.append(", %").append(_if_true_label).append(" ], [ %").append(falseNode);
            buffer.append(", %").append(_if_false_label).append(" ]\n");
            //OJO: esto funciona para 1 nivel de anidación, no sé si se replica para más!
            _if_true_label = ifEnd_name;
            _if_false_label = ifEnd_name;
            
            //Guardar resultado:
            _locals.put(ifResult_name, resultType);
            returnId = ifResult_name;
        }
        else if(e instanceof NewExpr) 
        {
            NewExpr newExpr = (NewExpr)e;

            String var_name = generateVariableId();
            for(int i = 0; i < _classes.size(); i++) {
                if(_classes.get(i).getName().equals(newExpr.getType())) {
                    buffer.append("%").append(var_name).append(" = call ").append(coolToLlvmType(newExpr.getType()));
                    buffer.append(" @new").append(newExpr.getType()).append("()\n");
                    _locals.put(var_name, newExpr.getType());
                }
            }
            returnId = var_name;
        }
        else if(e instanceof UnaryExpr) {
            UnaryExpr expr = (UnaryExpr)e;

            String value_node = readNode(expr.getValue(), scope, buffer);
            String var_name = generateVariableId();

            if(value_node != null) {

                switch(expr.getOp()) {

                    case ISVOID:
                        break;

                    case NEGATE:
                        buffer.append("%").append(var_name);
                        buffer.append(" = call i32 @_negate(i32 %").append(value_node).append(")\n");
                        _locals.put(var_name, "Int");
                        break;

                    case NOT:
                        buffer.append("%").append(var_name);
                        buffer.append(" = call i1 @_not(i1 %").append(value_node).append(")\n");
                        _locals.put(var_name, "Bool");
                        break;

                    default:
                        throw new RuntimeException("Invalid unary op");
                }
                returnId = var_name;
            }
        }
        else if(e instanceof BinaryExpr) {
            BinaryExpr expr = (BinaryExpr)e;


            String left_node = readNode(expr.getLeft(), scope, buffer);
            String right_node = readNode(expr.getRight(), scope, buffer);
            String var_name = generateVariableId();

            if(left_node != null && right_node != null) {

                switch(expr.getOp()) {

                    
                    case PLUS:
                        buffer.append("%").append(var_name);
                        buffer.append(" = call i32 @_add(i32 %").append(left_node).append(", i32 %");
                        buffer.append(right_node).append(")\n");
                        _locals.put(var_name, "Int");
                        break;

                    case MINUS:
                        buffer.append("%").append(var_name);
                        buffer.append(" = call i32 @_substract(i32 %").append(left_node).append(", i32 %");
                        buffer.append(right_node).append(")\n");
                        _locals.put(var_name, "Int");
                        break;
                        
                    case MULT:
                        buffer.append("%").append(var_name);
                        buffer.append(" = call i32 @_multiply(i32 %").append(left_node).append(", i32 %");
                        buffer.append(right_node).append(")\n");
                        _locals.put(var_name, "Int");
                        break;

                    case DIV:
                        buffer.append("%").append(var_name);
                        buffer.append(" = call i32 @_divide(i32 %").append(left_node).append(", i32 %");
                        buffer.append(right_node).append(")\n");
                        _locals.put(var_name, "Int");
                        break;

                    case LT:
                        buffer.append("%").append(var_name);
                        buffer.append(" = call i1 @_less_than(i32 %").append(left_node).append(", i32 %");
                        buffer.append(right_node).append(")\n");
                        _locals.put(var_name, "Bool");
                        break;

                    case LTE:
                        buffer.append("%").append(var_name);
                        buffer.append(" = call i1 @_less_or_equals(i32 %").append(left_node).append(", i32 %");
                        buffer.append(right_node).append(")\n");
                        _locals.put(var_name, "Bool");
                        break;

                    case EQUALS:
                        if(expr.getLeft().getExprType().equals("Int")) {
                            buffer.append("%").append(var_name);
                            buffer.append(" = call i1 @_equals(i32 %").append(left_node).append(", i32 %");
                            buffer.append(right_node).append(")\n");
                            _locals.put(var_name, "Bool");
                            break;
                        }
                        else if(expr.getLeft().getExprType().equals("Bool")) {
                            buffer.append("%").append(var_name).append(" = icmp eq i1 %");
                            buffer.append(left_node).append(", %").append(right_node).append("\n");
                            _locals.put(var_name, "Bool");
                            break;
                        }
                        else if(expr.getLeft().getExprType().equals("String")) {
                            buffer.append("%").append(var_name);
                            buffer.append(" = call i32 @strcmp(i8* %").append(left_node).append(", i8* %");
                            buffer.append(right_node).append(")\n");
                            _locals.put(var_name, "Int");
                            String strcmp_result = generateVariableId();
                            buffer.append("%").append(strcmp_result);
                            buffer.append(" = call i1 @_equals(i32 %").append(var_name).append(", i32 0)\n");
                            _locals.put(strcmp_result, "Bool");
                            var_name = strcmp_result;
                            break;
                        }

                    default:
                        throw new RuntimeException("Invalid unary op");
                }
                returnId = var_name;
            }
        }
        else if (e instanceof CaseExpr) {
            // Nothing done here :(.
        }
        else if (e instanceof LetExpr) {
            // Nothing done here :(.
        }
        else if(e instanceof IdExpr) {
            /*
            Field entrega un scope. Su padre puede ser ClassScope, MethodScope u otra cosa.
            Si es ClassScope, es un field (global). Si es MethodScope, es un parámetro.
            Si es de los otros, es variable local directamente.
            */
            IdExpr id = (IdExpr)e;
            Field field = scope.getField(id.getId());
            
            if(field != null) {
                Scope parent = field.getParent();
                if(parent instanceof ClassScope) {
                    ClassScope c_parent = (ClassScope)parent;
                    String var_name;
                    String var_type = id.getExprType();

                    if(id.getId().equals("self")) {
                        var_name = "self";
                    }
                    else {
                        int attr_index = 0;
                        for(int i = 0; i < _classes.size(); i++) {
                            attr_index = _classes.get(i).getAttributeIndex(id.getId());
                            if(attr_index != -1) {
                                break;
                            }
                        }
                        String ptr_name = generateVariableId();
                        buffer.append("%").append(ptr_name).append(" = getelementptr inbounds ");
                        buffer.append(coolToLlvmType(c_parent.getClassType())).append(" %self");
                        buffer.append(", i32 0, i32 ").append(attr_index).append("\n");
                        if(var_type.equals("String")) {
                            int l = _str_lengths.get(id.getId());
                            var_name = generateStringId();
                            buffer.append("%").append(var_name).append(" = load i8** %").append(ptr_name).append("\n");
                            _locals.put(var_name, "String");
                            _str_lengths.put(var_name, l);
                        }
                        else {
                            var_name = generateVariableId();
                            buffer.append("%").append(var_name).append(" = load ").append(coolToLlvmType(var_type));
                            buffer.append("* %").append(ptr_name).append("\n");
                            _locals.put(var_name, var_type);
                        }
                    }
                    returnId = var_name;
                }
                else if(parent instanceof MethodScope) {
                    returnId = id.getId();
                }
                else {
                    returnId = id.getId();
                }
            }

        }
        else if(e instanceof ValueExpr) {
            Object value = ((ValueExpr)e).getValue();
            String var_name;

            if(value instanceof String) {
                String str_value = (String) value;
                int l = str_value.length() + 1;
                str_value = formatString(str_value);
                String global_var_name = generateStringId();
                _strings.append("@").append(global_var_name).append(" = internal constant [");
                _strings.append(l).append(" x i8] c\"").append(str_value).append("\\00\"\n");
                _str_lengths.put(global_var_name, l);
                var_name = generateStringId();
                buffer.append("%").append(var_name).append(" = bitcast [").append(l);
                buffer.append(" x i8]* @").append(global_var_name).append(" to i8*\n");
                _locals.put(var_name, "String");
                _str_lengths.put(var_name, l);
            }
            else if(value instanceof Integer) {
                Integer int_value = (Integer) value;
                var_name = generateVariableId();
                buffer.append("%").append(var_name).append(" = add i32 ").append(int_value.intValue()).append(", 0\n");
                _locals.put(var_name, "Int");
            }
            else if(value instanceof Boolean) {
                Boolean bool_value = (Boolean) value;
                var_name = generateVariableId();
                buffer.append("%").append(var_name).append(" = add i1 ");
                if(bool_value)
                    buffer.append("1");
                else
                    buffer.append("0");
                buffer.append(", 0\n");
            }
            else {
                throw new RuntimeException(String.format("Unsupported constant type %s\n", e.getClass()));
            }
            returnId = var_name;
        }
        else {

            if( e != null) {
                throw new RuntimeException(String.format("Unsupported node type %s\n", e.getClass()));
            }
            else {
                throw new RuntimeException(String.format("Null node", e.getClass()));
            }

        }

        if("SELF_TYPE".equals(returnId)) {

            ClassScope classScope = scope.getClassScope();

        }

        return returnId;

    }


    public void printErrors() {
        _symTable.printErrors();
    }

    public String coolToLlvmType(String cool_type) {
        if(cool_type.equals("Int"))
            return "i32";
        else if(cool_type.equals("Bool"))
            return "i1";
        else if(cool_type.equals("String"))
            return "i8*";
        else
            return "%" + cool_type + "*";
    }

    private String generateStringId() {
        String stringId = "llvm_string_" + _string_index;
        _string_index++;
        return stringId;
    }

    private String generateVariableId() {
        String varId = "llvm_variable_" + _variable_index;
        _variable_index++;
        return varId;
    }

    private String formatString(String s) {
        s = s.replace("\n", "\\0A");
        s = s.replace("\f", "\\0C");
        s = s.replace("\t", "\\09");
        s = s.replace("\b", "\\08");
        return s;
    }

    private String fixMainType(String t) {
        String type = t;
        if(t.equals("Main")) {
            type = "IO";
        }
        return type;
    }
}