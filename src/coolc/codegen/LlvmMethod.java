package coolc.codegen;

import coolc.ast.*;
import java.util.*;

public class LlvmMethod {
	private String _name;
	private int _index;

	public String getName() {
		return _name;
	}

	public int getIndex() {
		return _index;
	}

	public LlvmMethod(String name, int index) {
		_name = name;
		_index = index;
	}
}