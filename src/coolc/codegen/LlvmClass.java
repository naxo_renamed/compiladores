package coolc.codegen;

import coolc.ast.*;
import java.util.*;
import coolc.codegen.*;

public class LlvmClass {
    private String _name;
    private ArrayList<LlvmMethod> _methods;
    private ArrayList<LlvmAttribute> _attributes;

    public String getName() {
        return _name;
    }

    public LlvmClass(String name) {
        _name = name;
        _methods = new ArrayList<LlvmMethod>();
        _attributes = new ArrayList<LlvmAttribute>();
    }

    public void addMethod(String name) {
        LlvmMethod method = new LlvmMethod(name, _methods.size() + 1);
        _methods.add(method);
    }

    public int getMethodIndex(String name) {
        for(int i = 0; i < _methods.size(); i++) {
            if(_methods.get(i).getName().equals(name)) {
                return _methods.get(i).getIndex();
            }
        }
        return -1;
    }

    public void addAttribute(String name) {
        LlvmAttribute attribute = new LlvmAttribute(name, _attributes.size() + 1);
        _attributes.add(attribute);
    }

    public int getAttributeIndex(String name) {
        for(int i = 0; i < _attributes.size(); i++) {
            if(_attributes.get(i).getName().equals(name)) {
                return _attributes.get(i).getIndex();
            }
        }
        return -1;
    }
}